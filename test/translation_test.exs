defmodule TranslationTest do
    use ExUnit.Case

    @valid_po  "test/fixtures/valid.po"
    @pt_BR %Language{
        short_name: "pt_BR",
        long_name: "Brazilian Portuguese"
    }
    @package %Package{name: "Elixmology"}

    test "return valid translations." do
        translations = Elixmology.parse(
            @pt_BR,
            @package,
            @valid_po
            )
        assert length(translations.entries) > 0
        assert translations.language == @pt_BR
        assert translations.package == @package
    end
end