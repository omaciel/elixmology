defmodule EntityTest do
    use ExUnit.Case

    @pt_BR %Language{
        short_name: "pt_BR",
        long_name: "Brazilian Portuguese"
    }
    @package %Package{name: "Elixmology"}

    test "create an empty `Language` entity." do
        assert %{short_name: nil, long_name: nil} = %Language{}
    end

    test "create `Language` entity only providing short_name." do
        assert %{short_name: "pt_BR"} = %Language{short_name: "pt_BR"}
    end

    test "create `Language` entity only providing long_name." do
        assert %{long_name: "Brazilian Portuguese"} 
          = %Language{long_name: "Brazilian Portuguese"}
    end

    test "create `Package` entity." do
        assert %{name: "Elixmology"} = %Package{name: "Elixmology"}
    end

    test "create `Translation` entity with `Package` and `Language`." do
        translation = %Translation{
            language: @pt_BR,
            package: @package,
        }
        assert %{language: @pt_BR} = translation
        assert %{package: @package} = translation
    end
end