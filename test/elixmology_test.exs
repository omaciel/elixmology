defmodule ElixmologyTest do
  use ExUnit.Case

  doctest Elixmology

  @empty_po "test/fixtures/empty.po"
  @ghost_po "test/fixtures/foo.po" # This does not exist
  @invalid_po "test/fixtures/invalid.po"
  @valid_po  "test/fixtures/valid.po"

  test "parses valid po file" do
    assert {:ok, po} = Elixmology.open_file(@valid_po)
    assert length(po.translations) > 0
  end

  test "parse non-existing po file." do
      assert {:error, :enoent} = Elixmology.open_file(@ghost_po)
  end

  test "parses empty po file" do
    assert {:ok, po} = Elixmology.open_file(@empty_po)
    assert length(po.translations) == 0
  end

  test "parses invalid po file" do
    assert {:error, line, reason} = Elixmology.open_file(@invalid_po)
    assert is_integer(line)
    assert is_binary(reason)
  end

end
