defmodule Elixmology do
  @moduledoc """
  Documentation for Elixmology.
  """

  alias Gettext.PO

  @doc """
  Parses a translation *.po file.
  """
  @spec parse(
      language :: String.t(),
      package :: String.t(),
      file_name :: String.t()) :: list
  def parse(language, package, file_name) do
    entries = open_file(file_name)
    |> read_translations
    |> Enum.map(&create_translation_entry/1)

    %TranslationList{
        language: language,
        package: package,
        entries: entries
    }
  end

  @spec open_file(
          binary
          | maybe_improper_list(
              binary | maybe_improper_list(any, binary | []) | char,
              binary | []
            )
        ) :: {:error, atom} | {:error, pos_integer, binary}
  def open_file(file_name) do
    PO.parse_file(file_name)
  end

  @spec read_translations({:ok, atom | %{translations: any}}) :: any
  def read_translations({:ok, po}) do
      po.translations
  end

  @spec create_translation_entry(atom | %{msgid: any, msgstr: any}) :: Translation.t()
  def create_translation_entry(entry) do
    %Translation{
        msgid: entry.msgid,
        msgstr: entry.msgstr
    }
  end
end