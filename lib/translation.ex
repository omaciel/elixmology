defmodule Translation do
    defstruct [
        package: %Package{},
        language: %Language{},
        msgid: "",
        msgstr: "",
    ]
end