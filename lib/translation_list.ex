defmodule TranslationList do

  defstruct language: "", package: "", entries: []

  def new(language \\ nil, package \\ nil, entries \\ []) do
      %TranslationList{
          language: language,
          package: package,
          entries: entries,
        }
  end

  def add_entry(entries_list, entry) do
    entry = %Translation{
        language: entries_list.language,
        package: entries_list.package,
        msgid: entry.msgid,
        msgstr: entry.msgstr
    }
    %TranslationList{entries_list | entries: [entry | entries_list.entries]}
  end

  def entries(entries_list) do
      entries_list.entries
  end

end